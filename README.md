## Table of content ##


1. Database and Diagrams.

    | Name folder / File | Description                                    |
    :--- |  :------------|
    TAXISRCP E-R  Final.jpg|
    TAXISRCP-FA.html |  [TAXISRCP-FA](https://gitlab.com/faroamroa/taxis-rcp-project/-/blob/master/Databases%20and%20Diagrams/TAXISRCP-FA.html)
    Conten of Database |
    Diagram Information|  1.1 Basic Information, 1.2 Diagram Description  1.3. Diagram Annotation     ![MOdel  E-R n](https://gitlab.com/faroamroa/taxis-rcp-project/-/blob/master/Databases%20and%20Diagrams/TAXISRCP%20E-R%20%20Final.jpg)
    Load_data.ipynb  | connect to Postgres  and load all tables and create new views.

  


    1. Tables Scripts.
           
    | Name folder / File | Description                                    |
    :--- |  :------------
    direccion_depuration.ipynb| 	Debugging of direction tables for geocoding
    get-polygon-localidad.ipynb| Use Shapely to create the polygon
    get-veredas.ipynb  | get veredas of pasto with adress
    mapa-try1.html |
    shapefile_mapdraft.ipynb

2. Dash Board_taxisrcp.


    | Name folder / File | Description                                    |
    :--- |  :------------
    assets | styles and images
    data / | flat files and dataframes
    df_corregimiento_total.csv | filtered information of the district
    df_localidad_total.csv | filtered information of the locality
    urban.geojson| geographic information file
    lib / | python modules (.py)
    __pycache__ | 	dash map pasto
    cancellation.py| cancellation analisys component
    map_pasto.py | map of pasto component
    request.py | Request component
    requestpredict.py|  prediction component
    sidebar.py | slider bar
    title.py| title of dashboard
    app.py |   Main APP definition
    index.py |  Basics Requirements, and aplicaction layaout 

3. Tables of Scripts.

    | Name folder / File | Description                                    |
    :--- |  :------------
    direccion_depuration.ipynb |	Debugging of direction tables for geocoding 
    get-polygon-localidad.ipynb |	
    get-veredas.ipynb |
    pastomodelo-inprocess.ipynb| 	

3. Inputs.

    | Name folder / File | Description                                    |
    :--- |  :------------
    Precipittacion_temperatura.csv|Datos_Hidrometeorol_gicos_Crudos_Red_de_Estaciones_IDEAM___
    calendario_ds4a.csv| 
    Precipitaci_n.csv|


4 taxi_project_EDA_modelling.ipynb.

|  Description                                    |
:--- |
The demand for taxis could depend of various features or variables: available taxis or cars, the weather(some temperatures could increased the  propensity of taking a car service), places of Pasto (it's possible to expect that certain places have more demand for taxis than others) hours, days and holidays. About this ultimate variables, it's possible to understand the demand for taxis like a data series with season, trend and residual. In that case, some of the propertys or assumptions of the OLS model are violated, so, it's necessary to take care specially of the trend that could cause correlation on the residuals.  In order to model the demand for taxis, it's necessary to build the database that contains all the mentioned variables. It is very important to point out that some variables of seasonality and climate were downloaded by the team from pages such as datosabiertos.gov.co.
model_data¬ Model storage.
taxi_project_EDA_modelling.ipynb|Notebook EDA_modelling.

5 Notebook Files.

|Name |  Description                                    |
:--- | :-----|
cancelation_analysis.ipynb |Analysis of cancellation requests made as reasons, hours, averages 
main_query.ipynb |  Connection to dashboard de Postgres and preparation of consultation for generation of graphics.
taxi_project_EDA_modelling.ipynb|  This uses ase -Squared Error (https://en.wikipedia.org/wiki/Mean_squared_error) , The algorithm to optimize, root means squared is useful for regression
Geocding_api_google.ipynb | Prepara addres for get latitude and longitude.  You should use the your own key of Google Cloud Platform.

5 Api files.

|Name |  Description                                    |
:--- | :-----|
api_rf.py | Api Scripts
forecasting.csv| Data Base for prediction
modelrf.data | Random forest model data
request_rf.py |  Scripts for request 


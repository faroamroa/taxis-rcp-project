# -*- coding: utf-8 -*-
import base64
from flask import Flask
from flask import request
from flask import jsonify
import numpy as np
import pandas as pd
import xgboost as xgb
import joblib
import json

# Global variables
dl_model = "C:/Users/PLANEACION/Documents/DS4A/notebooks/taxis-rcp-project/api_taxisrcp/modelrf.data"
model = joblib.load(dl_model)

# API from Flask instance
api = Flask(__name__)


@api.route('/api/status', methods=['GET'])
def status():
    """
    GET method for API status verification.
    """
    
    message = {
        "status": 200,
        "message": [
            "This API is up and running!"
        ]
    }
    response = jsonify(message)
    response.status_code = 200

    return response


@api.route('/api/predict', methods=['POST'])
def predict():
    """
    POST method for emotion prediction endpoint.
    """
    
    # Get data as JSON from POST
    data = request.get_json()

    test = pd.DataFrame(data["data"], index = [0,])

    prediction = model.predict(test)
 
    message = {
        "status": 200,
        "message": [
            {
                "task": "Prediction Transaction",
                "prediction": str(prediction[0])
            }
        ]
    }
    response = jsonify(message)
    response.status_code = 200

    return response


@api.errorhandler(404)
def not_found(error=None):
    """
    GET method for not found routes.
    """
    
    message = {
        "status": 404,
        "message": [
            "[ERROR] URL not found."
        ]
    }
    response = jsonify(message)
    response.status_code = 404
    
    return response


if __name__ == '__main__':
    api.run(port=5000, debug=True)


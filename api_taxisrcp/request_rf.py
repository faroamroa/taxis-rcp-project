import requests
import json
import pandas as pd
import base64

date_req = "2020-01-31"
year_req = int(date_req[0:4])
month_req = int(date_req[5:7])
day_req = int(date_req[8:])

forecasting = pd.read_csv("C:/Users/PLANEACION/Documents/DS4A/notebooks/taxis-rcp-project/api_taxisrcp/forecasting.csv")
cols = ['servicios', 'temperatura','precipitacion', 'year', 'month', 'day']
df_clean = pd.concat([forecasting[cols], pd.get_dummies(forecasting['year']), pd.get_dummies(forecasting['month']),pd.get_dummies(forecasting['tipo_dia2']),pd.get_dummies(forecasting['transp_season']),pd.get_dummies(forecasting['day_of_week'])], axis=1)
df_clean = df_clean[(df_clean["year"] == year_req) & (df_clean["month"] == month_req) & (df_clean["day"] == day_req)]
df_clean = df_clean.drop(["year", "month", "day"], axis =1)
df_clean

data_request = df_clean.drop('servicios', axis=1)
data_request['lunes']=df_clean.iloc[:,24]
data_request['martes']=df_clean.iloc[:,25]
data_request['miercoles']=df_clean.iloc[:,26]
data_request['jueves']=df_clean.iloc[:,27]
data_request['viernes']=df_clean.iloc[:,28]
data_request['sabado']=df_clean.iloc[:,29]
data_request['domingo']=df_clean.iloc[:,30]
data_request = pd.concat([data_request.iloc[:,0:24], data_request.loc[:,"domingo"], data_request.loc[:,"jueves"], data_request.loc[:,"lunes":"miercoles"], data_request.loc[:,"sabado"], data_request.loc[:,"viernes"]], axis = 1)
data_request
# dic_req = df_req.to_dict(orient="list")
# data_request = pd.DataFrame(dic_req, index=[0,])
# dic_req
def check_status(url, verbose=True, outfile=False):
    """Function to verify the API status.
    
    Parameters
    ----------
    url : str
        The endpoint to verify the API status.
    verbose : bool
        A flag to print the text output.
    outfile : bool
        A flag to save the text output into a JSON file.
    
    Returns
    -------
    response : dict
        The response as a JSON object.
    """
    r = requests.get(url)
    response = r.json()
    
    if outfile:
        save_output(r.text, 'check_status.json')
    
    if verbose:
        print(r.text)

    return response

import json


def predict_emotion(url, data, verbose=True, outfile=False):
    """Function to call the emotion predictor API.
    Parameters
    ----------
    data : dataframe
        The row with the features to evaluate.
    url : str
        The endpoint to verify the API status.
    verbose : bool
        A flag to print the text output.
    outfile : bool
        A flag to save the text output into a JSON file.
    
    Returns
    -------
    response : dict
        The response as a JSON object.
    """
    
    data = {
        "data": data.to_dict(orient="list")
    }
    r = requests.post(url, json=data)
    #, data = json.dumps(data), headers = headers)
    
    response = r.json()
    
    if outfile:
        save_output(r.text, 'predict.json')
    
    if verbose:
        print(r.text)

    return response

print('=== API status check ===')
r = check_status('http://localhost:5000/api/status')


print('=== API Prediction Transactions ===')
p = predict_emotion('http://localhost:5000/api/predict', data_request.iloc[[0]])
print(p)

# print("La prediccion es: ", p["message"]["prediction"])


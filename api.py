import base64
from flask import Flask
from flask import Request
from flask import jsonify

import statsmodels.formula.api as sfm
import joblib
import json

dlmodel = "/model.data"
model = joblib.load(dlmodel)

# Api from flask instance
api = Flask(__name__)

@api.route("/api/status", methods = ["GET"])
def status():
    """
    GET method for api status verification
    """

    message = {
        "status": 200,
        "message": [
            "The API is up and running"
        ]
    }
    response = jsonify(message)
    response.status_code = 200

    return response


@api.route('/api/predict', methods=['POST'])
def predict():
    """
    POST method for emotion prediction endpoint.
    """
    
    # Get data as JSON from POST
    data = request.get_json()
    
    # Build input vector for DL model
    test = pd.DataFrame(data['data'])
    
    # Predict using DL model
    prediction = model.predict(test)
    
    # Send response
    message = {
        "status": 200,
        "message": [
            {
                "task": "Prediction Transaction",
                "prediction": str(prediction[0])
            }
        ]
    }
    response = jsonify(message)
    response.status_code = 200

    return response


@api.errorhandler(404)
def not_found(error=None):
    """
    GET method for not found routes.
    """
    
    message = {
        "status": 404,
        "message": [
            "[ERROR] URL not found."
        ]
    }
    response = jsonify(message)
    response.status_code = 404
    
    return response


if __name__ == '__main__':
    api.run(port=5000, debug=True)
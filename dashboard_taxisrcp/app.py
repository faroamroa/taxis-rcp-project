#######################################################
# Main APP definition.
#
# Dash Bootstrap Components used for main theme and better
# organization.
#######################################################
import pathlib
import os
import pandas as pd
import json
import dash
import dash_bootstrap_components as dbc
#######################################################
# Imports for Map
import io
from unicodedata import normalize
from sqlalchemy import create_engine
import numpy as np
import math
# Imports for api
from flask import Flask
from flask import request
from flask import jsonify
import joblib
import json
from flask_restful import Resource, Api

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])
app.title = "Pronóstico de Demanda de Solicitudes - TAXIS-RCP SAS" 

server = app.server

# We need this for function callbacks not present in the app.layout
app.config.suppress_callback_exceptions = True

# server = Flask('my_app')
# app = dash.Dash(server=server)
api = Api(server)

#################
# Global variables
dl_model = "C:/Users/PLANEACION/Documents/DS4A/notebooks/taxis-rcp-project/api_taxisrcp/modelrf.data"
model = joblib.load(dl_model)

# API from Flask instance
api = Flask(__name__)


@api.route('/api/status', methods=['GET'])
def status():
    """
    GET method for API status verification.
    """
    
    message = {
        "status": 200,
        "message": [
            "This API is up and running!"
        ]
    }
    response = jsonify(message)
    response.status_code = 200

    return response


@api.route('/api/predict', methods=['POST'])
def predict():
    """
    POST method for emotion prediction endpoint.
    """
    
    # Get data as JSON from POST
    data = request.get_json()

    test = pd.DataFrame(data["data"], index = [0,])

    prediction = model.predict(test)
 
    message = {
        "status": 200,
        "message": [
            {
                "task": "Prediction Transaction",
                "prediction": str(prediction[0])
            }
        ]
    }
    response = jsonify(message)
    response.status_code = 200

    return response


@api.errorhandler(404)
def not_found(error=None):
    """
    GET method for not found routes.
    """
    
    message = {
        "status": 404,
        "message": [
            "[ERROR] URL not found."
        ]
    }
    response = jsonify(message)
    response.status_code = 404
    
    return response
##################

# get relative data folder
PATH = pathlib.Path(__file__).parent
DATA_DIR = PATH.joinpath("data").resolve()


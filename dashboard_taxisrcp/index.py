# Basics Requirements
import pathlib
import os
import dash
from dash.dependencies import Input, Output, State, ClientsideFunction
from dash.exceptions import PreventUpdate
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import plotly.express as px
from sqlalchemy import create_engine
# Dash Bootstrap Components
import dash_bootstrap_components as dbc
# Data
import math
import numpy as np
import datetime as dt
import pandas as pd
import json
# Recall app
from app import app

###########################################################
# APP LAYOUT:
###########################################################
# LOAD THE DIFFERENT FILES
from lib import title, sidebar, cancellation, request, requestpredict, map_pasto

# PLACE THE COMPONENTS IN THE LAYOUT

app.layout = dbc.Container(
    [
        dbc.Row(dbc.Col(title.title_layout, className = 'class-title-container', md=12)),
        dbc.Row(
            [
                dbc.Col(sidebar.sidebar_layout, className = 'class-sidebar-container'),
                dbc.Col([
                        dbc.Row(
                            [
                            dbc.Col(dbc.Tabs(
                                [
                                    dbc.Tab(label="Solicitudes", tab_id="solicitudes"),
                                    dbc.Tab(label="Cancelaciones", tab_id="cancelaciones"),
                                    dbc.Tab(label="Pronóstico", tab_id="pronostico"),
                                ], id="tabs", active_tab="solicitudes"), md= 11, width={"order": 1}, className = 'class-tabs-container'),

                            # dbc.Col(dbc.Button("API", color="success"), md=1, width={"order": 2}, className = 'class-button')
                            ], no_gutters = True
                                ),
                        html.Div(id="tab-content")
                    ], className="class-main-container"
                )
            ], className="class-body-container"
        )
    ], fluid=True, className="class-app-container"
)
###############################################################
# SERVICIO SIDEBAR INTERACTION
#################################################################
@app.callback(
    Output("tab-content", "children"),
    [Input("tabs", "active_tab")])

def render_tab_content(active_tab):
    if active_tab is not None:
        if active_tab == "solicitudes":
            return (
                dbc.Row(
                    [
                        dbc.Col(
                            dbc.Row(
                                dbc.Col([
                                    dbc.Row(html.Div(request.requestbyyear_layout, className =  'class-requestbyyear-container')),
                                    dbc.Row(html.Div(request.requestbyhour_layout, className = 'class-requestbyhour-container'))
                                        ])
                                        ), md=6, width={"order": 1}
                                        ),
                        dbc.Col(map_pasto.map_pasto_layout, md=5, width={"order": 2, 'offset': 1}, className = 'class-map-container')
                    ], no_gutters = True),
                dbc.Row(dbc.Col(request.requestbyyymm_layout, className = 'class-requestbyyymm-container', md= 12)),
                )
        elif active_tab == "cancelaciones":
            return (
                dbc.Row(dbc.Col(cancellation.reason_cancellation_layout, className = 'hour_rangeslider-container', md= 12)),
                dbc.Row([
                    dbc.Col(cancellation.hour_cancellation_layout, className = 'class-cancelation-container', md=6, width={"order": 1}),
                    dbc.Col(cancellation.avghour_cancellation_layout, className = 'class-cancelation-container', md=6, width={"order": 2})
                        ], no_gutters = True
                        )
            )
        elif active_tab == "pronostico":
            return (
                dbc.Row(dbc.Col(requestpredict.requestpredict_layout, className = 'class-requestpredict-container', md=12)),
                dbc.Row(dbc.Col(
                    html.H5("El gráfico muestra la comparación entre los servicios solicitados y la predicción realizada \
                            a través de un modelo Random Forest o bosque aleatorio con 200 estimadores. Este modelo usa las \
                            variables de temperatura, precipitación, y estacionalidad mensual y diaria para realizar la \
                            predicción con un error cuadrático medio de 378 que corresponde al 20% del promedio \
                            diario de servicios."), md=8, width={"offset": 1})
                ))
        else:
            return "No tab selected"


@app.callback(
    Output("popover", "is_open"),
    [Input("popover-target", "n_clicks")],
    [State("popover", "is_open")]
)
def toggle_popover(n, is_open):
    if n:
        return not is_open
    return is_open


@app.callback(
    [
        Output("requestbyyymm_id", "figure"),
        Output("requestbyhour_id", "figure"),
        Output("requestbyyear_id", "figure"),
    ],
    [
        Input("date_picker_id", "start_date"),
        Input("date_picker_id", "end_date")
    ]
)
def update_dashboard(start_date, end_date):
    if start_date is None or end_date is None:
        raise PreventUpdate
    else:
        engine = create_engine('postgresql://postgres:ds4ataxis@taxis-rcp.cmtjjmtp7rqw.us-east-1.rds.amazonaws.com:5432/taxisrcp') 
        conn = engine.connect()
        sql_request="select anomesdia, anomes, hora, ano, sum(cantidad) as cantidad from solicitudesn group by anomesdia, anomes, hora, ano"
        df_request =pd.read_sql_query(sql_request, conn)
        df_request = df_request[(df_request["anomesdia"] >= start_date) & (df_request["anomesdia"] < end_date)].copy()

        df4 = df_request.groupby(['anomes']).agg({'cantidad':'sum'}).round(1).reset_index()
        df5 = df_request.groupby(['hora']).agg({'cantidad':'sum'}).round(1).reset_index()
        df6 = df_request.groupby(['ano']).agg({'cantidad':'sum'}).round(1).reset_index()

        fig4 = px.line(df4, x='anomes', y='cantidad',
                    hover_data=['anomes', 'cantidad'], 
                    labels={'anomes':'Año-Mes','cantidad':'Solicitudes'}, 
                    height=400)
        # fig4.update_traces(texttemplate = '%{text: .2s}', textposition ='outside')
        fig4.update_layout(xaxis_tickangle = 90)
        fig4.update_layout(font=dict(family="Arial",size=12), title_font_color="grey")

        fig5 = px.bar(df5, x='hora', y='cantidad',
                    hover_data=['hora', 'cantidad'],
                    labels={'hora':'Hora','cantidad':'Solicitudes'}, 
                    height=400)
        # fig5.update_traces(texttemplate = '%{text: .2s}', textposition ='outside')
        fig5.update_layout(xaxis_tickangle = 90)
        fig5.update_xaxes(tick0=0, dtick=1)
        fig5.update_layout(font=dict(family="Arial",size=12), title_font_color="grey")

        fig6 = px.bar(df6, x='ano', y='cantidad',
                    hover_data=['ano', 'cantidad'],
                    labels={'ano':'Año','cantidad':'Solicitudes'}, 
                    height=400)
        # fig6.update_traces(texttemplate = '%{text: .2s}', textposition ='outside')
        fig6.update_layout(xaxis_tickangle = 90)
        fig6.update_layout(font=dict(family="Arial",size=12), title_font_color="grey")


        return [fig4, fig5, fig6]


###############################################################
###############################################################
if __name__ == "__main__":
    app.run_server(debug=True)
    # app.run_server(host="0.0.0.0", port="8050")

# Basics Requirements
import dash
from dash.dependencies import Input, Output, State, ClientsideFunction
from dash.exceptions import PreventUpdate
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import plotly.express as px
# Data
import math
import numpy as np
import datetime as dt
import pandas as pd
# Recall app
from app import app, df, geozonas

###############################################################
# Load and modify the data that will be used.
#################################################################
dff = df[df['zona'] == "tr"].copy()
#############################
# Create the maps
#############################
dff_map = dff[dff['cuenca_txt'] != "No"].reset_index().copy()
dff_map = dff_map.groupby(['cuenca_txt']).agg({'duracion_min':'mean'}).reset_index()

map  =  px.choropleth_mapbox(dff_map,
        locations='cuenca_txt',
        color='duracion_min',
        geojson=geozonas,
        zoom=9.5,
        mapbox_style="carto-positron",
        center={"lat": 4.656183720555693, "lon": -74.11481005158143},
        color_continuous_scale=[(0, "green"), (0.3, "orange"),(1, "red")],
        opacity=0.5
        )
# map.update_layout(height=650)
map.update_layout(title="Permanencia Promedio en Estaciones de Cuencas <br> (Portales y Estaciones Intermedias) <br> ", title_x=0.5)
map.update_layout(font=dict(family="Arial",size=12), title_font_color="grey")
map.update_layout(height=750)
##############################
# Maps Layout
##############################
map_layout= html.Div(
    # Place the main graph component here:
    dcc.Graph(figure=map, id="map_id"),
    # dcc.Graph(style = {'height':'95vh'},figure=map, id="map_id")
    className="class-map"
)

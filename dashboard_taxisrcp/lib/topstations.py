# Basics Requirements
import dash
from dash.dependencies import Input, Output, State, ClientsideFunction
from dash.exceptions import PreventUpdate
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import plotly.express as px
# Data
import math
import numpy as np
import datetime as dt
import pandas as pd
# Recall app
from app import app, df

###############################################################
# Load and modify the data that will be used.
#################################################################
dff = df[df['zona'] == "tr"].copy()
#############################
# Create the figures
#############################
df_top = dff.groupby(['estacion_txt']).agg({'duracion_min':'mean'}).sort_values(['duracion_min'], ascending=False).reset_index()
fig3 = px.bar(df_top[:20], x='estacion_txt', y='duracion_min', text='duracion_min')
fig3.update_traces(texttemplate='%{text:.2s}', textposition='inside')
fig3.update_traces(marker_color='rgb(158,202,225)', marker_line_color='rgb(8,48,107)', marker_line_width=1.5, opacity=0.6)
fig3.update_layout(title="Top 20 Portales/Estaciones por Permanencia", title_x=0.5)
fig3.update_layout(font=dict(family="Arial",size=12), title_font_color="grey")
fig3.update_layout(plot_bgcolor='rgb(235,235,235)')
# fig3.update_xaxes(title_text='Portal/Estación')
fig3.update_xaxes(title_text='')
fig3.update_xaxes(tickangle = 45)
fig3.update_yaxes(title_text='Tiempo Promedio (min)')
fig3.update_layout(height=350)
#################################################################################
# Here the layout for the plots to use.
#################################################################################

topstations_layout = html.Div(
    [
        # Place the different graph components here.
        dcc.Graph(figure=fig3, id="top_id")
    ],
    className="class-topstations"
)
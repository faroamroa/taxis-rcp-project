# Basics Requirements
import dash
from dash.dependencies import Input, Output, State, ClientsideFunction
import dash_core_components as dcc
import dash_html_components as html
# Dash Bootstrap Components
import dash_bootstrap_components as dbc
# Data
from sqlalchemy import create_engine
import datetime as dt
import pandas as pd
# Recall app
from app import app

engine = create_engine('postgresql://postgres:ds4ataxis@taxis-rcp.cmtjjmtp7rqw.us-east-1.rds.amazonaws.com:5432/taxisrcp') 
conn = engine.connect()
sql_request="select anomesdia, anomes, hora, ano, sum(cantidad) as cantidad from solicitudesn group by anomesdia, anomes, hora, ano"
df_request =pd.read_sql_query(sql_request, conn)
conn.close()
#############################################################################
# Imagenes
#############################################################################
taxis_rcp_image = html.Div(
    children=[html.Img(src=app.get_asset_url("taxis-rcp-sas_image.svg"), id="taxis_rcp-image", width = "80%")], className = "class-taxisrcp_image",
)

ds4a_image = html.Div(
    children=[html.Img(src=app.get_asset_url("ds4a2.svg"), id="ds4a-image", width = "100%")], className = "class-ds4a_image",
)
##############################################################################
# Date Picker
##############################################################################
date_picker = dcc.DatePickerRange(
    id="date_picker_id",
    min_date_allowed=df_request['anomesdia'].min(),
    max_date_allowed=df_request['anomesdia'].max(),
    start_date=df_request['anomesdia'].min(),
    end_date=df_request['anomesdia'].max(),
    start_date_placeholder_text="Inicio",
    end_date_placeholder_text="Fin",
    display_format='DD/MM/YY',
    day_size = 30
)
#############################################################################
# Tipo día Dropdown
#############################################################################
periodo_dropdown = dcc.Dropdown(
    id="tipodia_drop_id",
        options=[
            {'label': 'Hora', 'value': 'hour'},
            {'label': 'Año', 'value': 'year'},
            {'label': 'Año-Mes', 'value': 'year_month'},
            {'label': 'Mes', 'value': 'month'},
            {'label': 'Día', 'value': 'weekday'}
        ],
    style={'color': 'black'},
    value=['hour']
)

#############################################################################
# Hora Slicer
#############################################################################
hour_rangeslider =  html.Div([
    dbc.Row([
            dbc.Col(html.Div("Rango Horario"), md= 2),
            dbc.Col(dcc.RangeSlider(
            id="hour_rangeslider_id",
            marks={i: '{}'.format(i) for i in range(4, 24)},
            min=4,
            max=24,
            value=[6, 8]), md= 10
            )
        ], no_gutters = True),
    ]
)



popover = html.Div(
    [
        dbc.Button(
            "Información", id="popover-target", color="success"
        ),
        dbc.Popover(
            [
                dbc.PopoverHeader("Desarrollado Por TEAM-98:"),
                dbc.PopoverBody("Brayan Rojas - Lína Maria Peña - Fernando Roa - Cristian Ríos - Johanna Lozano - Carlos A Puerto V"),
            ],
            id="popover",
            is_open=False,
            target="popover-target",
        ),
    ]
)
#############################################################################
# Sidebar Layout
#############################################################################
sidebar_layout = html.Div(
    [
        taxis_rcp_image,
        html.Hr(),
        html.H5("Rango de Fechas"),
        date_picker,
        html.Hr(),
        # html.H5("Cancelaciones"),
        # html.H5("Periodo de Análisis"),
        # periodo_dropdown,
        # html.Hr(),
        ds4a_image,
        html.Hr(),
        popover

    ],
    className="class-sidebar",
)

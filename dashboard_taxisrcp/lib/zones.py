# Basics Requirements
import dash
from dash.dependencies import Input, Output, State, ClientsideFunction
from dash.exceptions import PreventUpdate
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import plotly.express as px
# Data
import math
import numpy as np
import datetime as dt
import pandas as pd
# Recall app
from app import app, df

###############################################################
# Load and modify the data that will be used.
#################################################################
dff = df[df['zona'] == "tr"].copy()
#############################
# Create the figures
#############################
df_zonas = dff.groupby(['troncal_txt']).agg({'duracion_min':'mean'}).round(1).sort_values(['duracion_min'], ascending=False).reset_index()
fig4 = px.bar(df_zonas, x='troncal_txt', y='duracion_min', text='duracion_min')
fig4.update_traces(texttemplate='%{text}', textposition='inside')
fig4.update_traces(marker_color='rgb(158,202,225)', marker_line_color='rgb(8,48,107)', marker_line_width=1.5, opacity=0.6)
fig4.update_layout(title="Permanencia por Troncal", title_x=0.5)
fig4.update_layout(font=dict(family="Arial",size=12), title_font_color="grey")
fig4.update_layout(plot_bgcolor='rgb(235,235,235)')
# fig4.update_xaxes(title_text='Troncal')
fig4.update_xaxes(title_text='')
fig4.update_yaxes(title_text='Tiempo Promedio (min)')
fig4.update_layout(height=350)

#################################################################################
# Here the layout for the plots to use.
#################################################################################
zones_layout = html.Div(
    # Place the different graph components here.
    dcc.Graph(figure=fig4, id="zone_id"), className="class-zones"
)
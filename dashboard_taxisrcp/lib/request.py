# Basics Requirements
import dash
from dash.dependencies import Input, Output, State, ClientsideFunction
from dash.exceptions import PreventUpdate
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import plotly.express as px
# Dash Bootstrap Components
import dash_bootstrap_components as dbc
# Data
import math
import numpy as np
import datetime as dt
import pandas as pd

from sqlalchemy import create_engine
from datetime import datetime
from unidecode import unidecode
import nltk
from six.moves import configparser
from collections import Counter
# Recall app
from app import app

engine = create_engine('postgresql://postgres:ds4ataxis@taxis-rcp.cmtjjmtp7rqw.us-east-1.rds.amazonaws.com:5432/taxisrcp') 
conn = engine.connect()

##########################################################
# paths
##########################################################
## Query summary by year Month
sql_request="select anomesdia, anomes, hora, ano, sum(cantidad) as cantidad from solicitudesn group by anomesdia, anomes, hora, ano"

df_request =pd.read_sql_query(sql_request, conn)
df4 = df_request.groupby(['anomes']).agg({'cantidad':'sum'}).round(1).reset_index()
df5 = df_request.groupby(['hora']).agg({'cantidad':'sum'}).round(1).reset_index()
df6 = df_request.groupby(['ano']).agg({'cantidad':'sum'}).round(1).reset_index()

########################################
# Query create fig 2, summaru by yyyy-mm
########################################

fig4 = px.line(df4, x='anomes', y='cantidad',
             hover_data=['anomes', 'cantidad'], 
             labels={'anomes':'Año-Mes','cantidad':'Solicitudes'}, 
             height=400)
# fig4.update_traces(texttemplate = '%{text: .2s}', textposition ='outside')
fig4.update_layout(xaxis_tickangle = 90)
fig4.update_layout(font=dict(family="Arial",size=12), title_font_color="grey")

fig5 = px.bar(df5, x='hora', y='cantidad',
             hover_data=['hora', 'cantidad'],
             labels={'hora':'Hora','cantidad':'Solicitudes'}, 
             height=400)
# fig5.update_traces(texttemplate = '%{text: .2s}', textposition ='outside')
fig5.update_layout(xaxis_tickangle = 90)
fig5.update_xaxes(tick0=0, dtick=1)
fig5.update_layout(font=dict(family="Arial",size=12), title_font_color="grey")

fig6 = px.bar(df6, x='ano', y='cantidad',
             hover_data=['ano', 'cantidad'],
             labels={'ano':'Año','cantidad':'Solicitudes'}, 
             height=400)
# fig6.update_traces(texttemplate = '%{text: .2s}', textposition ='outside')
fig6.update_layout(xaxis_tickangle = 90)
fig6.update_layout(font=dict(family="Arial",size=12), title_font_color="grey")


conn.close()

##### >>>>>>>>>> >>>>>>>>>> >>>>>>>>>> >>>>>>>>>>  #####
# Figures Layout
##### >>>>>>>>>> >>>>>>>>>> >>>>>>>>>> >>>>>>>>>>  #####
requestbyyymm_layout = html.Div(
    dcc.Graph(figure=fig4, id="requestbyyymm_id"), className="class-requestbyyymm"
)

requestbyhour_layout = html.Div(
    dcc.Graph(figure=fig5, id="requestbyhour_id"), className="class-requestbyhour"
)

requestbyyear_layout = html.Div(
    dcc.Graph(figure=fig6, id="requestbyyear_id"), className="class-requestbyyear"
)

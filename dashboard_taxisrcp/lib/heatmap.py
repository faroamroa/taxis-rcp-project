# Basics Requirements
import dash
from dash.dependencies import Input, Output, State, ClientsideFunction
from dash.exceptions import PreventUpdate
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import plotly.express as px
# Data
import math
import numpy as np
import datetime as dt
import pandas as pd
# Recall app
from app import app, df

###############################################################
# Load and modify the data that will be used.
#################################################################
dff = df[df['zona'] == "tr"].copy()

portales_list = ['PORTAL 20 DE JULIO','PORTAL AMERICAS','PORTAL DE LA 80','PORTAL DE SUBA','PORTAL DE USME',
            'PORTAL DEL NORTE','PORTAL DEL SUR','PORTAL DEL TUNAL','PORTAL EL DORADO']

df_heatmap = dff[dff['estacion_txt'].isin(portales_list)].copy()
#############################
# Create the maps
#############################
df_heatmap = df_heatmap.groupby(['estacion_txt','hora']).agg({'duracion_min':'mean'}).reset_index()
fig5 = px.density_heatmap(df_heatmap, 
                          x="hora", 
                          y='estacion_txt', 
                          z='duracion_min', 
                          nbinsx=19,
                          histfunc = 'avg',
                          color_continuous_scale=[(0, "green"), (0.3, "orange"),(1, "red")]
                          )
fig5.update_xaxes(tick0=4, dtick=1)
# fig5.update_layout(height = 350, width = 1000)
# fig5.update_xaxes(title_text='Hora')
fig5.update_xaxes(title_text='')
fig5.update_yaxes(title_text='Portal')
fig5.update_layout(title="Permanencia en Portales por Franja Horaria", title_x=0.5)
fig5.update_layout(font=dict(family="Arial",size=12), title_font_color="grey")
fig5.update_layout(height=350)
##############################
# Maps Layout
##############################
heatmap_layout= html.Div(
    # Place the main graph component here:
    dcc.Graph(figure=fig5, id="heatmap_id"), className="class-heatmap"
)
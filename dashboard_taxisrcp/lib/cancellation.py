# Basics Requirements
import dash
from dash.dependencies import Input, Output, State, ClientsideFunction
from dash.exceptions import PreventUpdate
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import plotly.express as px
# Dash Bootstrap Components
import dash_bootstrap_components as dbc
# Data
import math
import numpy as np
import datetime as dt
import pandas as pd

from sqlalchemy import create_engine
from datetime import datetime
from unidecode import unidecode
import nltk
from six.moves import configparser
from collections import Counter
import textwrap
from pandas.api.types import CategoricalDtype
# Recall app
from app import app

##### >>>>>>>>>> >>>>>>>>>> >>>>>>>>>> >>>>>>>>>>  #####
# Load and modify the data that will be used.
##### >>>>>>>>>> >>>>>>>>>> >>>>>>>>>> >>>>>>>>>>  #####
engine = create_engine('postgresql://postgres:ds4ataxis@taxis-rcp.cmtjjmtp7rqw.us-east-1.rds.amazonaws.com:5432/taxisrcp') 
conn = engine.connect()
data_cancel = pd.read_sql_query('select * from mastermind_solicitudcancelada', con = conn)


data_cancel.reset_index(inplace = True)
# Determine how many repetitions have each "solicitud_id" along the dataset
rep = []
indexes = []
solicitud_id_new = []
for index, value in enumerate(data_cancel["solicitud_id"]):
    solicitud_id_new.append(value)
    filtered_id = [id for id in solicitud_id_new if id == value]
    rep.append(len(filtered_id))
    indexes.append(index)

value_rep = [value for value in rep if value > 1]
data_cancel = pd.concat([data_cancel, pd.DataFrame(rep, columns = ["rep"])], axis = 1)
data_cancel = data_cancel.loc[(data_cancel["rep"] == 1) & (data_cancel["motivo_cancelacion"] != "")  & (pd.notnull(data_cancel["creado"]))].reset_index()
data_cancel["motivo_cancelacion"] = data_cancel["motivo_cancelacion"].apply(lambda x: str(x) + ".")
data_cancel["length_reason_cance"] = data_cancel["motivo_cancelacion"].apply(lambda x: len(x))
data_cancel["length_reason_cance"].mean()

motivo_joined = ' '.join(data_cancel["motivo_cancelacion"])

nltk.download('punkt')
motivo_tokenized = nltk.sent_tokenize(motivo_joined)

# Top 10 reasons of cancellation
top_sentence = Counter(motivo_tokenized).most_common(10)

data_reasons = pd.DataFrame(top_sentence, columns = ["motivo_cancelacion", "quantity"])
data_reasons = data_reasons.astype({"motivo_cancelacion":'category'})

# Wrapping long "motivo_concelacion" strings
data_reasons["motivo_cancelacion"] = data_reasons["motivo_cancelacion"].apply(lambda x: "<br>".join(textwrap.wrap(x, width= 12)))
df1 = data_reasons.copy()

##############################################################
#############################################################################################

data_cancel["date"] = data_cancel["creado"].apply(lambda x: pd.to_datetime(x[0:11]) if pd.notna(x) else x)
data_cancel["year"] = data_cancel["date"].dt.year
years = [x for x in data_cancel["year"].unique() if pd.notna(x)]
data_cancel["year"] = data_cancel["year"].astype(
    CategoricalDtype(years, ordered = True))
data_cancel["month"] = data_cancel["date"].dt.month.astype(
    CategoricalDtype(range(1,13,1), ordered = True))
data_cancel["day"] = data_cancel["date"].dt.day.astype(
    CategoricalDtype(range(1,32,1), ordered = True))
data_cancel["weekday1"] = data_cancel["date"].dt.weekday.astype(
    CategoricalDtype(range(0,7,1), ordered = True))
data_cancel["weekday"] = data_cancel["weekday1"].apply(lambda x: int(x) + 1 if pd.notna(x) else x)
data_cancel["hour"] = data_cancel["creado"].apply(lambda x: pd.to_datetime(x[11:]) if pd.notna(x) else x).dt.hour.astype(
    CategoricalDtype(range(0,24,1), ordered = True))
data_cancel["year_month"] = data_cancel["creado"].apply(lambda x: x[0:7] if pd.notna(x) else x)

# Dataset grouped para conteos
group_unit = "hour" # year, year_month, month, weekday or hour
df2 = data_cancel.groupby([group_unit]).agg({"id":"count"})
df2.reset_index(inplace = True)
df2.rename(columns = {"id":"quantity"}, inplace=True)

##############################################################
#############################################################################################
# Dataset grouped to means
group_unit = "hour" # year, year_month, month, weekday or hour
def to_group(x):
    return {
        "month": ["year", "month"],
        "weekday": ["date", "weekday"],
        "hour": ["date", "hour"]
    }.get(x, [x])

to_group(group_unit)

df3 = data_cancel.groupby(to_group(group_unit), as_index = False)["id"].count()
df3.rename(columns = {"id":"quantity"}, inplace=True)
df3 = df3.groupby([group_unit], as_index = False).agg({"quantity":"mean"})
df3["quantity"] = round(df3["quantity"], 1)
##### >>>>>>>>>> >>>>>>>>>> >>>>>>>>>> >>>>>>>>>>  #####
# Create the figures
##### >>>>>>>>>> >>>>>>>>>> >>>>>>>>>> >>>>>>>>>>  #####
fig1 = px.bar(df1, x = "motivo_cancelacion", y = "quantity", text = "quantity", title = "Top 10 Motivos de Cancelación", labels={"motivo_cancelacion":"Motivos Cancelación", "quantity" : "Cantidad"}, height= 500)
fig1.update_traces(textposition = 'outside')
fig1.update_layout(uniformtext_minsize=7, title_x=1, xaxis_tickangle = 0)
fig1.update_layout(font=dict(family="Arial",size=12), title_font_color="grey")

##########################################################################################################
fig2 = px.bar(df2, x = group_unit, y = "quantity", text = "quantity", title = f"Número de Cancelaciones por {group_unit.capitalize()}", labels={group_unit:group_unit.capitalize(), "quantity" : "Quantity"}, height= 500, hover_data = [group_unit], color = "quantity")
fig2.update_traces(textposition = 'outside')
fig2.update_layout(uniformtext_minsize=5, title_x=0.5, xaxis_tickangle = np.where(group_unit == "year_month", 90, 0)*1)
fig2.update_xaxes(type='category')
fig2.update_layout(font=dict(family="Arial",size=12), title_font_color="grey")
##########################################################################################################
fig3 = px.bar(df3, x = group_unit, y = "quantity", text = "quantity", title = f"Promedio de Cancelaciones por {group_unit.capitalize()}", labels={group_unit:group_unit.capitalize(), "quantity" : "Quantity"}, height= 500, hover_data = [group_unit], color = "quantity")
fig3.update_traces(textposition = 'outside')
fig3.update_layout(uniformtext_minsize = 5, title_x = 0.5, xaxis_tickangle = np.where(group_unit == "year_month", 90, 0)*1)
fig3.update_xaxes(type = 'category')
fig3.update_layout(font=dict(family="Arial",size=12), title_font_color="grey")

conn.close()
##### >>>>>>>>>> >>>>>>>>>> >>>>>>>>>> >>>>>>>>>>  #####
# Figures Layout
##### >>>>>>>>>> >>>>>>>>>> >>>>>>>>>> >>>>>>>>>>  #####

reason_cancellation_layout = html.Div(
    dcc.Graph(figure=fig1, id="reason_cancelacion_id"), className="class-cancelacion"
)

hour_cancellation_layout = html.Div(
    dcc.Graph(figure=fig2, id="hour_cancelacion_id"), className="class-cancelacion"
)

avghour_cancellation_layout = html.Div(
    dcc.Graph(figure=fig3, id="avghour_cancelacion_id"), className="class-cancelacion"
)
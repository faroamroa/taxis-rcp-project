# Basics Requirements
import dash
from dash.dependencies import Input, Output, State, ClientsideFunction
import dash_core_components as dcc
import dash_html_components as html
# Recall app
from app import app

# title layout
title_layout = html.Div(
    className="class-title",
    children=[
        html.H2("Pronóstico de Demanda de Solicitudes - TAXIS-RCP SAS")
    ],
    id="title_id",
)



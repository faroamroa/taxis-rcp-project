# Basics Requirements
import dash
from dash.dependencies import Input, Output, State, ClientsideFunction
from dash.exceptions import PreventUpdate
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import plotly.express as px
# Dash Bootstrap Components
import dash_bootstrap_components as dbc
# Data
import math
import numpy as np
import datetime as dt
import pandas as pd

from sqlalchemy import create_engine
from datetime import datetime
from unidecode import unidecode
import nltk
from six.moves import configparser
from collections import Counter
# Recall app
from app import app

engine = create_engine('postgresql://postgres:ds4ataxis@taxis-rcp.cmtjjmtp7rqw.us-east-1.rds.amazonaws.com:5432/taxisrcp') 
conn = engine.connect()

### Query Model prediction

sql7="SELECT year, month, sum(servicios) as serviciosx  ,sum(prediccion_randomforest) as  synew_rfox  \
FROM   forecasting1   \
GROUP BY  year, month \
order by year, month "

df7=pd.read_sql_query(sql7,conn)

df7["yearmonth"] = df7.year.astype(str)+"-"+df7.month.astype(str)
df7.rename(columns={'serviciosx':'Solicitudes','synew_rfox':'Pronóstico'}, inplace = True)

########################################
# Query create fig 6, Prediction MOdel
########################################

fig7 = px.line(df7, x="yearmonth", 
                    y=["Solicitudes","Pronóstico"],
                    height=500,
                    labels={'yearmonth':'Año-Mes'}
                    )
fig7.show()

conn.close()

##### >>>>>>>>>> >>>>>>>>>> >>>>>>>>>> >>>>>>>>>>  #####
# Figures Layout
##### >>>>>>>>>> >>>>>>>>>> >>>>>>>>>> >>>>>>>>>>  #####
requestpredict_layout = html.Div(
    dcc.Graph(figure=fig7, id="requestpredict_id"), className="class-requestpredict"
)

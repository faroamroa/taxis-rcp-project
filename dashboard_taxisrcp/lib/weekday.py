# Basics Requirements
import dash
from dash.dependencies import Input, Output, State, ClientsideFunction
from dash.exceptions import PreventUpdate
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import plotly.express as px
# Dash Bootstrap Components
import dash_bootstrap_components as dbc
# Data
import math
import numpy as np
import datetime as dt
import pandas as pd
# Recall app
from app import app, df

###############################################################
# Load and modify the data that will be used.
#################################################################
dff = df[df['zona'] == "tr"].copy()
#############################
# Create the figures
#############################
df_week = dff.groupby(['weekday']).agg({'duracion_min':'mean'}).round(1).reset_index()
fig2 = px.scatter(df_week, x='weekday', y='duracion_min', size='duracion_min')
fig2.update_traces(texttemplate='%{text}', textposition='top center')
fig2.update_traces(marker_color='rgb(158,202,225)', marker_line_color='rgb(8,48,107)', marker_line_width=1.5, opacity=0.6)
fig2.update_layout(title="Permanencia por Día de la Semana", title_x=0.5)
fig2.update_layout(font=dict(family="Arial",size=12), title_font_color="grey")
fig2.update_layout(plot_bgcolor='rgb(235,235,235)')
fig2.update_layout(xaxis = dict(tickmode = 'array', tickvals = [0, 1, 2, 3, 4, 5, 6],  ticktext = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo']))
# fig2.update_xaxes(title_text='Día')
fig2.update_xaxes(title_text='')
fig2.update_yaxes(title_text='Tiempo Promedio (min)')
fig2.update_layout(height=350)

##############################
# Figures Layout
##############################

weekday_layout = html.Div(
    dcc.Graph(figure=fig2, id="weekday_id"), className="class-weekday"
)
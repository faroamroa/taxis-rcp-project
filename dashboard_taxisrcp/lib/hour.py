# Basics Requirements
import dash
from dash.dependencies import Input, Output, State, ClientsideFunction
from dash.exceptions import PreventUpdate
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import plotly.express as px
# Dash Bootstrap Components
import dash_bootstrap_components as dbc
# Data
import math
import numpy as np
import datetime as dt
import pandas as pd
# Recall app
from app import app, df

###############################################################
# Load and modify the data that will be used.
#################################################################
dff = df[df['zona'] == "tr"].copy()
#############################
# Create the figures
#############################
df_hour = dff.groupby(['hora', 'tipo_dia_txt']).agg({'duracion_min':'mean'}).reset_index()
fig1 = px.line(df_hour, x='hora', y='duracion_min', color = 'tipo_dia_txt')
#fig1.update_traces(texttemplate='%{text:.2s}', textposition='outside')
fig1.update_traces(marker_color='rgb(158,202,225)', marker_line_color='rgb(8,48,107)', marker_line_width=1.5, opacity=0.6)
fig1.update_layout(title="Permanencia por Franja Horaria", title_x=0.5)
fig1.update_layout(font=dict(family="Arial",size=12), title_font_color="grey")
fig1.update_layout(plot_bgcolor='rgb(235,235,235)')
#fig1.update_layout(legend=dict(yanchor="top", y=0.98,xanchor="left", x=0.88))
fig1.update_layout(legend=dict(orientation="h", yanchor="bottom", y=0.92, xanchor="right", x=1))
fig1.update_layout(legend_title_text='Tipo Día')
fig1.update_layout(height=350)
fig1.update_xaxes(tick0=0, dtick=1)
# fig1.update_xaxes(title_text='Hora')
fig1.update_xaxes(title_text='')
fig1.update_yaxes(title_text='Tiempo Promedio (min)')
##############################
# Figures Layout
##############################

hour_layout = html.Div(
    dcc.Graph(figure=fig1, id="hour_id"), className="class-hour"
)
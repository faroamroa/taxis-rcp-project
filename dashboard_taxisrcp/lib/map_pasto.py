#
# Basics Requirements
import dash
from dash.dependencies import Input, Output, State, ClientsideFunction
from dash.exceptions import PreventUpdate
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import plotly.express as px

from sqlalchemy import create_engine
import os
import json
import pathlib

# Data
import math
import numpy as np
import datetime as dt
import pandas as pd
from geojson import Feature, FeatureCollection, Polygon
import plotly.graph_objects as go

# Recall app
from app import app, DATA_DIR

##########################################################
# Extracción DataFrame Mapa con Localidades y Corregimientos
##########################################################

# Conexión a la base de datos
engine = create_engine('postgresql://postgres:ds4ataxis@taxis-rcp.cmtjjmtp7rqw.us-east-1.rds.amazonaws.com:5432/taxisrcp') 
conn = engine.connect()

# Extraer tablas df_corregimiento_total y df_localidad_total
df_corregimiento_total = DATA_DIR.joinpath("df_corregimiento_total.csv")

df_localidad_total = os.path.join(DATA_DIR, "df_localidad_total.csv")

df_corregimiento_total = pd.read_csv(df_corregimiento_total)
df_localidad_total = pd.read_csv(df_localidad_total)

# Obtener geojson para mapa
cpoblado_geojson = os.path.join(DATA_DIR, "cpoblado.geojson")
urban_cpoblado_geojson = os.path.join(DATA_DIR, "urban_cpoblado.geojson")
urban_geojson = os.path.join(DATA_DIR, "urban.geojson")

with open(cpoblado_geojson, encoding='utf-8') as geo:
    cpoblado_geojson = json.loads(geo.read())

with open(urban_geojson, encoding='utf-8') as geo:
    urban_geojson = json.loads(geo.read())

with open(urban_cpoblado_geojson, encoding='utf-8') as geo:
    urban_cpoblado_geojson = json.loads(geo.read())


##########################################################
# Codigo comentado
##########################################################

df_corregimiento_total['numero_solicitudes'] = df_corregimiento_total['numero_solicitudes'].fillna(0)
df_localidad_total['nombre_localidad'] = df_localidad_total['id_localidad'].apply(lambda x: 'Localidad '+ str(x))

# Get centros poblados
pasto_cpoblado = pd.DataFrame.copy(df_corregimiento_total)
pasto_cpoblado = pasto_cpoblado.iloc[1:46]
pasto_cpoblado = pasto_cpoblado.rename(columns={'id_corregimiento':'id', 'nombre_corregimiento':'nombre'})

# Get urban
pasto_urban = pd.DataFrame.copy(df_localidad_total)
pasto_urban = pasto_urban[0:27]
pasto_urban = pasto_urban.rename(columns={'id_localidad':'id', 'nombre_localidad':'nombre'})

# Get urban + centros poblados
pasto_urban_cpoblado = pasto_urban.append(pasto_cpoblado, ignore_index=True)

###############################################################
# Define required functions
###############################################################
# Style fuction for map
def style_fcn(x):
    return { 'color':'black','weight':0.5,'fillColor': None ,'fillOpacity':0}

# Labels popup function for map
def get_popup(layer_geom, df):
    for x in range(1,len(df)+1):
        temp = df.iloc[x-1:x]
        temp.to_file('temp.geojson', driver='GeoJSON')
        temp_geojson_layer = folium.GeoJson('temp.geojson',style_function=style_fcn)
        folium.Popup(df['nombre'].iloc[x-1]).add_to(temp_geojson_layer)
        temp_geojson_layer.add_to(layer_geom)
    return layer_geom

#############################
# Create the map
#############################

map = go.Figure(go.Choroplethmapbox(geojson=cpoblado_geojson, 
                                    locations=pasto_cpoblado.id, 
                                    z=pasto_cpoblado.numero_solicitudes,
                                    colorscale=[(0, "blue"), (0.1, "green"),(0.5, "orange"),(1, "red")],
                                    zmin=0, zmax=110903,
                                    marker_opacity=0.5, 
                                    marker_line_width=0))

map.add_trace(go.Choroplethmapbox(geojson=urban_cpoblado_geojson, 
                                    locations=pasto_urban_cpoblado.id, 
                                    z=pasto_urban_cpoblado.numero_solicitudes,
                                    colorscale=[(0, "blue"), (0.1, "green"),(0.5, "orange"),(1, "red")],
                                    zmin=0, zmax=110903,
                                    marker_opacity=0.5, 
                                    marker_line_width=0, 
                                    showscale=False))

map.update_layout(mapbox_style="carto-positron",
                  mapbox_zoom=12,
                  mapbox_center = {"lat": 1.211198, "lon": -77.277579})

# map.update_layout(height=650)
map.update_layout(title="Solicitudes por Zona ", title_x=0.5)
map.update_layout(font=dict(family="Arial",size=12), title_font_color="grey")
map.update_layout(height=750)

conn.close()



##############################
# Maps Layout
##############################

map_pasto_layout= html.Div(
    # Place the main graph component here:
    dcc.Graph(figure=map, id="map_id"),
    # dcc.Graph(style = {'height':'95vh'},figure=map, id="map_id")
    className="class-map"
)
